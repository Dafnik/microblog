<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

if (!file_exists(__DIR__ . '/../private/settings.json')) {
  header("Location: ./setup.php");
  die();
}

require_once(__DIR__ . '/logic/settingsHandler.php');
require_once(__DIR__ . '/logic/postHandler.php');
require(__DIR__ . '/logic/theming.php');

$typ = 'DEFAULT';

if (isset($_REQUEST['postID']) OR !empty($_REQUEST['postID'])) {
  $typ = 'showSinglePost';
}

if (isset($_REQUEST['s']) OR !empty($_REQUEST['s'])) {
  $typ = 'findInPost';
  $toSearch = $_REQUEST['s'];
}

if (isset($_REQUEST['userID']) OR !empty($_REQUEST['userID'])) {
  $typ = 'findPostsByUser';
}

if (isset($_REQUEST['d']) OR !empty($_REQUEST['d'])) {
  $typ = 'showDate';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo getSettings()['blogName'] ?></title>

  <link rel="shortcut icon" type="image/favicon.ico" href="">

  <link href="styles/style.css" type="text/css" rel="stylesheet"/>
  <?php
  if ($theme == 'dark') {
    echo '<link href="styles/dark.css" type="text/css" rel="stylesheet"/>';
  } else if ($theme == 'light') {
    echo '<link href="styles/light.css" type="text/css" rel="stylesheet"/>';
  }
  ?>
</head>

<body>
<header>
  <h1><a href="index.php"><?php echo getSettings()['blogName'] ?></a></h1>
  <?php
  switch ($theme) {
    case "light":
      echo '<a href="./index.php?theme=dark">Licht aus!</a>';
      break;

    case "dark":
      echo '<a href="./index.php?theme=light">Licht an!</a>';
      break;
  }
  ?>
  <form action="index.php" method="get" style="margin-bottom: 2.5vh">
    <input type="text" placeholder="Suchbegriff" name="s" id="s" aria-label="Suche nach einem Wort"
           required value="<?php if ($typ == 'findInPost') echo $toSearch; ?>">
    <button type="submit">Suchen</button>
  </form>
</header>

<?php
switch ($typ) {
  case 'showSinglePost':
    $postID = $_REQUEST['postID'];

    $post = getPostPerID($postID);
    if ($post != null) {
      $date = new DateTime($post['createDate']);
      echo '<h2>Einzelner Beitrag:</h2>';
      echo '<div class="postEntry">';
      echo '<h3><a href="index.php?postID=' . $post['postID'] . '" target="_blank">[]</a> ' . $date->format('l, d. m Y') . '</h3>';
      echo stylePost($post['postContent'], $post['username'], $post['userID'], $post['createDatetime']);
      echo '</div>';
    } else {
      echo 'Dieser Beitrag wurde nicht gefunden!!';
    }

    break;

  case 'findInPost':
    $posts = findInPosts($toSearch);

    echo '<h2>Zeige Beiträge mit dem Keyword "' . $toSearch . '"</h2>';
    if ($posts != null) {
      foreach ($posts as $post) {
        echo '<div class="postEntry"><a href="index.php?postID=' . $post['postID'] . '" target="_blank">[]</a> ';
        echo stylePost($post['postContent'], $post['username'], $post['userID'], $post['createDatetime']);
        echo '</div>';
      }
    } else {
      echo 'Es wurde kein Beitrag mit dem Keyword gefunden!';
    }

    break;

  case 'findPostsByUser':
    $posts = findPostByUserID($_REQUEST['userID']);

    if ($posts != null) {
      echo '<h2>Zeige alle Beiträge von ' . $posts[0]['username'] . '</h2>';
      foreach ($posts as $post) {
        echo '<div class="postEntry"><a href="index.php?postID=' . $post['postID'] . '" target="_blank">[]</a> ';
        echo stylePost($post['postContent'], $post['username'], $post['userID'], $post['createDatetime']);
        echo '</div>';
      }
    } else {
      echo 'Es wurde kein Benutzer gefunden!';
    }

    break;

  case 'showDate':
    $date = $_REQUEST['d'];

    echo '<h3>Monat: ' . $date . '</h3>';

    echo '<ul id="dayUl">';

    $days = getDaysPerMonth($date);

    if ($days) {
      foreach ($days as $day) {
        echo '<li class="dayEntry"><h3>';
        echo $day->format('l, d. m Y');
        echo '</h3>';
        echo '<ul>';

        $posts = getPostsPerDay($day);
        foreach ($posts as $post) {
          echo '<li class="postEntry"><a href="index.php?postID=' . $post['postID'] . '" target="_blank">[]</a> ';
          echo stylePost($post['postContent'], $post['username'], $post['userID'], $post['createDatetime']);
          echo '</li>';
        }

        echo '</ul></li>';
      }

      echo '</ul>';
    } else {
      echo 'Es wurde nichts gefunden!';
    }

    break;

  default:
    echo '<ul id="dayUl">';

    $days = getDaysForMainTimeline();

    foreach ((array)$days as $day) {
      echo '<li class="dayEntry"><h3>';
      echo $day->format('l, d.m.Y');
      echo '</h3>';
      echo '<ul>';

      $posts = getPostsPerDay($day);
      foreach ($posts as $post) {
        echo '<li class="postEntry"><a href="index.php?postID=' . $post['postID'] . '" target="_blank">[]</a> ';
        echo stylePost($post['postContent'], $post['username'], $post['userID'], $post['createDatetime']);
        echo '</li>';
      }

      echo '</ul></li>';
    }

    echo '</ul>';

    break;
}

if ($typ == 'showDate') {
  $date = $_REQUEST['d'];

  if ($date != null) {
    $currentDate = DateTime::createFromFormat('Y-m-d', $date . '-1');
    $currentDate = $currentDate->format('Y-m');

    $nextMonth = date('Y-m', strtotime('+1 month', strtotime($currentDate)));

    $prevMonth = date('Y-m', strtotime('-1 month', strtotime($currentDate)));
  }
} else {
  $currentDate = new DateTime();
  $currentDate = $currentDate->format('Y-m');

  $nextMonth = date('Y-m', strtotime('+1 month'));

  $prevMonth = date('Y-m', strtotime('-1 month'));
}

?>

<div style="float: left; padding-bottom: 15px;">
  <a href="index.php?d=<?php echo $currentDate ?>">Ganzer Monat</a><br>
  <a href="index.php?d=<?php echo $prevMonth ?>">Vorheriger Monat</a><br>
  <a href="index.php?d=<?php echo $nextMonth ?>">Nächster Monat</a>
</div>

<div style="float:right; padding-bottom: 15px;">
  <a href="privacypolicy.php" target="_blank">Privacy policy</a>
  <br>
  <a href="imprint.php" target="_blank">Impressum</a>
</div>

</body>