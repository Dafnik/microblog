<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

require_once(__DIR__ . '/logic/settingsHandler.php');
require_once(__DIR__ . '/logic/authHandler.php');

$message = null;

if (isset($_REQUEST['logout'])) {
  logout();
  $message = "logoutSuccessful";
}

if (isLoggedIn()) {
  header('Location: ./admin.php');
  die();
}

if (isset($_REQUEST['username']) AND isset($_REQUEST['password']) AND !empty($_REQUEST['username']) AND
  !empty($_REQUEST['password'])) {

  $success = login($_REQUEST['username'], $_REQUEST['password']);
  if ($success) {
    header('Location: ./admin.php');
    die();
  } else {
    $message = "credentialsWrong";
  }
}

if (isset($_REQUEST['message']) AND !empty($_REQUEST['message'])) {
  $message = $_REQUEST['message'];
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login - <?php echo getSettings()['blogName'] ?></title>

  <link rel="shortcut icon" type="image/favicon.ico" href="">
</head>
<body>
<a href="index.php">Back to the blog</a>
<br>
<?php
switch ($message) {
  case "credentialsWrong":
    echo '<b>The username or password is incorrect!</b>';
    break;

  case "loginFirst":
    echo '<b>Please login first!</b>';
    break;

  case "logoutSuccessful":
    echo '<b>Successful logged out!</b>';
    break;
}
?>
<br>
<form method="post" action="login.php">
  Username:<br>
  <input type="text" id="username" name="username" placeholder="Username" required><br>
  Password: <br>
  <input type="password" id="password" name="password" placeholder="Password" required><br>

  <button type="submit">Log in</button>
</form>
</body>
</html>
