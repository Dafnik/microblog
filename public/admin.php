<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

if (!file_exists(__DIR__ . '/../private/settings.json')) {
  header("Location: ./setup.php");
  die();
}

require_once(__DIR__ . '/logic/settingsHandler.php');
require_once(__DIR__ . '/logic/authHandler.php');

if (!isLoggedIn()) {
  header("Location: ./login.php?message=loginFirst");
  die();
}

require_once(__DIR__ . '/logic/postHandler.php');

$action = null;
$message = null;
if (isset($_REQUEST['action']) AND isset($_REQUEST['action'])) {
  $action = $_REQUEST['action'];
}

switch ($action) {
  case "createPost":
    $postContent = $_REQUEST['postContent'];
    $userID = $_SESSION['userID'];

    createNewPost($postContent, $userID);
    $message = 'Successfully created this post!';
    break;

  case "deletePost":
    $postID = $_REQUEST['postID'];
    deletePost($postID);
    $message = 'Successfully deleted this post!';

    break;

  case "editPost":
    $postID = $_REQUEST['postID'];
    $postContent = $_REQUEST['postContent'];
    updatePost($postID, $postContent);
    $message = 'Successfully updated this post!';

    break;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin - <?php echo getSettings()['blogName'] ?></title>

  <link rel="shortcut icon" type="image/favicon.ico" href="">

  <link href="styles/bootstrap.min.css" type="text/css" rel="stylesheet"/>

  <style>
    blockquote {
      border-radius: 25px;
      padding: 20px;
      background-color: #E0E0E0;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php" target="_blank">Back to <?php echo getSettings()['blogName'] ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Posts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Users</a>
      </li>
    </ul>
    <form class="my-2 my-lg-0">
      <a class="btn btn-danger my-2 my-sm-0" href="login.php?logout=true">Logout</a>
    </form>
  </div>
</nav>
<div class="container">
  <?php
  if ($message != null) {
    echo '
    <div class="alert alert-success" role="alert" style="margin-top: 10px;">
      ' . $message . '
    </div>
    ';
  }
  ?>

  <button data-toggle="modal" data-target="#createPostModal" class="btn btn-success" style="margin-top: 10px;">Write a
    new post
  </button>

  <?php

  $posts = getAllPosts();

  foreach ($posts as $post) {
    $post['postContent'] = preg_replace('/(<a href="[^"]+")>/is', '\\1 target="_blank" rel="noopener">', $post['postContent']);

    echo '
    <div class="row" style="margin-top: 10px;">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            ' . $post['postContent'] . '
          </div>
          <div class="card-footer text-muted">
            ' . $post['username'] . ' | ' . $post['createDatetime'] . '
            
            <div style="float: right">
              <a href="editPost.php?postID=' . $post['postID'] . '" class="btn btn-warning">Edit</a>
              <a href="admin.php?action=deletePost&postID=' . $post['postID'] . '" class="btn btn-danger">Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    ';
  }

  ?>
</div>

<!-- Modal -->
<div class="modal fade" id="createPostModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Write a post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="admin.php">
        <div class="modal-body">
          <div class="row" style="padding-left: 10px; padding-right: 10px;">
            <textarea name="postContent" class="form-control col-12" id="postContent" placeholder="Write here your post"
                      required></textarea>
          </div>

          <div class="row" style="padding-left: 10px; padding-right: 10px; margin-top: 10px;">
            Preview:
          </div>

          <div class="row" style="padding-left: 10px; padding-right: 10px; margin-top: 10px;">
            <div style="border-color: gray; border-style: dashed" class="col-12" id="postContentPreview"></div>
          </div>
        </div>

        <input type="hidden" name="action" value="createPost">

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create this post</button>
        </div>
      </form>
    </div>
  </div>
</div>

</body>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="application/javascript">
  var postContent = document.getElementById('postContent');
  if (postContent.addEventListener) {
    postContent.addEventListener('input', function () {
      var preview = document.getElementById('postContentPreview');
      preview.innerHTML = postContent.value;
    }, false);
  } else if (postContent.attachEvent) {
    postContent.attachEvent('onpropertychange', function () {
      var preview = document.getElementById('postContentPreview');
      preview.innerHTML = postContent.value;
    });
  }
</script>
</html>