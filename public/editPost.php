<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

if (!file_exists(__DIR__ . '/../private/settings.json')) {
  header("Location: ./setup.php");
  die();
}

require_once(__DIR__ . '/logic/settingsHandler.php');
require_once(__DIR__ . '/logic/authHandler.php');

if (!isLoggedIn()) {
  header("Location: ./login.php?message=loginFirst");
  die();
}

require_once(__DIR__ . '/logic/postHandler.php');

if (isset($_REQUEST['postID']) AND !empty($_REQUEST['postID'])) {
  $post = getPostPerID($_REQUEST['postID']);
} else {
  header('Location: ./admin.php');
  die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin - <?php echo getSettings()['blogName'] ?></title>

  <link rel="shortcut icon" type="image/favicon.ico" href="">

  <link href="styles/bootstrap.min.css" type="text/css" rel="stylesheet"/>

  <style>
    blockquote {
      border-radius: 25px;
      padding: 20px;
      background-color: #E0E0E0;
    }
  </style>
</head>
<body>
<div class="container">
  <h1>Edit Post</h1>

  <form method="post" action="admin.php">
    <div class="modal-body">
      <div class="row" style="padding-left: 10px; padding-right: 10px;">
        <textarea name="postContent" class="form-control col-12" id="postContent" placeholder="Write here your post"
                  required><?php echo $post['postContent']; ?></textarea>
      </div>

      <div class="row" style="padding-left: 10px; padding-right: 10px; margin-top: 10px;">
        Preview:
      </div>

      <div class="row" style="padding-left: 10px; padding-right: 10px; margin-top: 10px;">
        <div style="border-color: gray; border-style: dashed" class="col-12"
             id="postContentPreview"><?php echo $post['postContent']; ?></div>
      </div>
    </div>

    <input type="hidden" name="postID" value=" <?php echo $post['postID']; ?>">
    <input type="hidden" name="action" value="editPost">

    <div class="modal-footer">
      <a href="admin.php" class="btn btn-secondary">Back</a>
      <button type="submit" class="btn btn-primary">Save this post</button>
    </div>
  </form>
</div>
</body>
<script type="application/javascript">
  var postContent = document.getElementById('postContent');
  if (postContent.addEventListener) {
    postContent.addEventListener('input', function () {
      var preview = document.getElementById('postContentPreview');
      preview.innerHTML = postContent.value;
    }, false);
  } else if (postContent.attachEvent) {
    postContent.attachEvent('onpropertychange', function () {
      var preview = document.getElementById('postContentPreview');
      preview.innerHTML = postContent.value;
    });
  }
</script>
</html>