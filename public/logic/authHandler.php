<?php
require_once(__DIR__ . '/databaseHandler.php');

function isLoggedIn()
{
  if (!isset($_SESSION)) {
    session_start();
  }

  if (!isset($_SESSION['isLoggedIn'])) {
    $_SESSION['isLoggedIn'] = false;
  }

  return $_SESSION['isLoggedIn'];
}

function login($username, $password)
{
  if (!isset($_SESSION)) {
    session_start();
  }

  $conn = getDatabaseConnection();

  $stmt = $conn->prepare('SELECT userID FROM users WHERE username = :username AND password = :password');
  $stmt->bindParam(':username', $username);
  $stmt->bindParam(':password', $password);
  $stmt->execute();
  $user = $stmt->fetch()[0];

  if ($user != null) {

    $_SESSION['userID'] = $user;
    $_SESSION['isLoggedIn'] = true;

    return true;
  } else {
    $_SESSION['isLoggedIn'] = false;
    $_SESSION['userID'] = null;

    return false;
  }
}

function logout()
{
  if (!isset($_SESSION)) {
    session_start();
  }

  $_SESSION = null;

  session_destroy();
}

function getUserID()
{
  if (isLoggedIn()) {
    return $_SESSION['userID'];
  } else {
    return null;
  }
}