<?php

require_once(__DIR__ . '/settingsHandler.php');

function getDatabaseConnection()
{
  $host = getSettings()['mysql_server'];
  $dbName = getSettings()['mysql_database'];
  $user = getSettings()['mysql_user'];
  $password = getSettings()['mysql_password'];
  $conn = new PDO ("mysql:host=$host;dbname=$dbName;charset=utf8", $user, $password);

  $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  return $conn;
}
