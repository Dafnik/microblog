<?php

require_once(__DIR__ . '/databaseHandler.php');
require_once(__DIR__ . '/settingsHandler.php');

function getDaysForMainTimeline()
{
  $conn = getDatabaseConnection();

  $postsToShow = getSettings()['posts_to_see_on_start_page'];

  $stmt = $conn->prepare('SELECT createDate FROM posts ORDER BY createDate DESC LIMIT :postsToShow;');
  $stmt->bindParam(':postsToShow', $postsToShow);
  $stmt->execute();
  $dates = $stmt->fetchAll();

  $toReturn = [];

  $previousDate = null;
  foreach ((array)$dates as $date) {
    $actualDate = new DateTime($date[0]);
    if ($previousDate == null) {
      $previousDate = $actualDate;

      array_push($toReturn, new DateTime($date[0]));
    }

    if ($actualDate < $previousDate) {
      array_push($toReturn, new DateTime($date[0]));

      $previousDate = $actualDate;
    }
  }

  return $toReturn;
}

function getDaysPerMonth($_month)
{
  if ($_month != null) {
    $conn = getDatabaseConnection();

    $year = substr($_month, 0, 4);
    $month = substr($_month, 5, 7);

    $stmt = $conn->prepare('SELECT createDate FROM posts WHERE MONTH(createDate) = :month AND YEAR(createDate) = :year ORDER BY createDate DESC;');
    $stmt->bindParam(':month', $month);
    $stmt->bindParam(':year', $year);
    $stmt->execute();
    $dates = $stmt->fetchAll();

    $toReturn = [];

    $previousDate = null;
    foreach ($dates as $date) {
      $actualDate = new DateTime($date['createDate']);
      if ($previousDate == null) {
        $previousDate = $actualDate;

        array_push($toReturn, new DateTime($date['createDate']));
      } else

        if ($actualDate < $previousDate) {
          array_push($toReturn, new DateTime($date['createDate']));

          $previousDate = $actualDate;
        }
    }

    return $toReturn;
  } else {
    return null;
  }
}

function getPostsPerDay($day)
{
  $conn = getDatabaseConnection();

  $dayFormat = $day->format('Y-m-d');

  $stmt = $conn->prepare('SELECT postID, postContent, createDate, createDatetime, username, userID FROM posts JOIN users u on posts.authorID = u.userID WHERE createDate=:date ORDER BY createDatetime DESC;;');
  $stmt->bindParam(':date', $dayFormat);
  $stmt->execute();
  return $stmt->fetchAll();
}

function getPostPerID($id)
{
  $conn = getDatabaseConnection();

  $stmt = $conn->prepare('SELECT postID, postContent, createDate, createDatetime, username, userID FROM posts JOIN users u on posts.authorID = u.userID WHERE postID=:postID;');
  $stmt->bindParam(':postID', $id);
  $stmt->execute();
  return $stmt->fetch();
}

function getAllPosts()
{
  $conn = getDatabaseConnection();

  $stmt = $conn->prepare('SELECT postID, postContent, createDate, createDatetime, username, userID FROM posts JOIN users u on posts.authorID = u.userID ORDER BY createDatetime DESC;');
  $stmt->execute();
  return $stmt->fetchAll();
}

function findInPosts($toSearch)
{
  if (!empty($toSearch)) {
    $conn = getDatabaseConnection();

    $stmt = $conn->prepare('SELECT postID, postContent, createDate, createDatetime, username, userID FROM posts JOIN users u on posts.authorID = u.userID ORDER BY createDatetime DESC;;');
    $stmt->execute();
    $posts = $stmt->fetchAll();

    $toReturn = [];

    foreach ($posts as $post) {
      if (strpos($post['postContent'], $toSearch) !== false) {
        array_push($toReturn, $post);
      }
    }

    return $toReturn;
  } else {
    return null;
  }
}

function findPostByUserID($userID)
{
  if (!empty($userID)) {
    $conn = getDatabaseConnection();

    $stmt = $conn->prepare('SELECT postID, postContent, createDate, createDatetime, username, userID FROM posts JOIN users u on posts.authorID = u.userID AND u.userID=:userID ORDER BY createDatetime DESC;');
    $stmt->bindParam(':userID', $userID);
    $stmt->execute();
    return $stmt->fetchAll();

  } else {
    return null;
  }
}

function stylePost($content, $username, $userID, $date)
{
  $content = preg_replace('/(<a href="[^"]+")>/is', '\\1 target="_blank" rel="noopener">', $content);

  $post = $content;

  $time = new DateTime($date);
  $time = $time->format('d/m/Y h:m:s');
  $post .= '<br><small>~ <a href="index.php?userID=' . $userID . '" target="_blank">' . $username . '</a> | '
    . $time . ' </small>';

  return $post;
}

function createNewPost($content, $userID)
{
  $conn = getDatabaseConnection();

  $stmt = $conn->prepare('INSERT INTO posts(postContent, createDatetime, createDate, authorID) VALUES (:content, now(), now(), :userID);');
  $stmt->bindParam(':content', $content);
  $stmt->bindParam(':userID', $userID);
  $stmt->execute();
}

function deletePost($postID)
{
  $conn = getDatabaseConnection();

  $stmt = $conn->prepare('DELETE FROM posts WHERE postID=:postID');
  $stmt->bindParam(':postID', $postID);
  $stmt->execute();
}

function updatePost($postID, $postContent)
{
  $conn = getDatabaseConnection();

  $stmt = $conn->prepare('UPDATE posts SET postContent=:postContent WHERE postID=:postID');
  $stmt->bindParam(':postID', $postID);
  $stmt->bindParam(':postContent', $postContent);
  $stmt->execute();
}