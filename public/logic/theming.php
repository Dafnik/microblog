<?php

$theme = "light";
if (!isset($_COOKIE["theme"])) {
  $cookie_value = "light";
  setcookie("theme", $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
} else {
  $theme = $_COOKIE["theme"];
}

if (isset($_REQUEST['theme'])) {
  if ($_REQUEST['theme'] == 'dark' OR $_REQUEST['theme'] == 'light') {
    setcookie("theme", $_REQUEST['theme'], time() + (86400 * 30), "/"); // 86400 = 1 day
    $theme = $_REQUEST['theme'];
  } else {
    setcookie("theme", "light", time() + (86400 * 30), "/"); // 86400 = 1 day
    $theme = "light";
  }
}