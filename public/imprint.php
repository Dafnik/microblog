<?php
require_once(__DIR__ . '/logic/settingsHandler.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo getSettings()['blogName'] ?></title>

  <link rel="shortcut icon" type="image/favicon.ico" href="">
</head>
<body style="padding: 10vh;">
<?php echo getSettings()['imprint']; ?>
</body>
</html>
