DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users(
  userID    INT           NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username  VARCHAR(256)  NOT NULL,
  password  VARCHAR (512) NOT NULL,
  loginKey  VARCHAR(4096),
  isAdmin   TINYINT       NOT NULL
);

CREATE TABLE IF NOT EXISTS posts(
  postID          INT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  postContent     TEXT      NOT NULL,
  createDatetime  DATETIME  NOT NULL,
  createDate      DATE      NOT NULL,
  authorID        INT       NOT NULL,

  FOREIGN KEY (authorID) REFERENCES users(userID) ON DELETE CASCADE
);
INSERT INTO users(username, password, isAdmin) VALUES ('Dafnik', 'Passwort1234', 1);

INSERT INTO posts(postContent, createDate, createDatetime, authorID)
VALUES ('Das ist ein Postcontent am 2018-11-28 08:32:11! <blockquote>Ur leiwond</blockquote> <a href="https://dafnik.me">Meine tolle Website</a> <a href="https://dafnik.me">Meine 2 tolle Website</a> Wow <br>Jetzt nach einem Break',
        '2018-11-28', '2018-11-28 08:32:11', 1);

--INSERT INTO posts(postContent, createDate, createDatetime, authorID) VALUES ('Das ist ein Postcontent am 2018-11-27 08:32:23!', '2018-11-27', '2018-11-27 08:32:23', 1);
--INSERT INTO posts(postContent, createDate, createDatetime, authorID) VALUES ('Das ist ein Postcontent am 2018-11-27 08:32:34!', '2018-11-27', '2018-11-27 08:32:34', 2);
--INSERT INTO posts(postContent, createDate, createDatetime, authorID) VALUES ('Das ist ein Postcontent am 2018-11-27 08:32:35!', '2018-11-27', '2018-11-27 08:32:35', 2);
--INSERT INTO posts(postContent, createDate, createDatetime, authorID) VALUES ('Das ist ein Postcontent am 2018-11-26 09:12:11!', '2018-11-26', '2018-11-26 09:32:11', 1);
--INSERT INTO posts(postContent, createDate, createDatetime, authorID) VALUES ('Das ist ein Postcontent am 2018-11-26 09:12:12!', '2018-11-26', '2018-11-26 09:32:12', 1);
--INSERT INTO posts(postContent, createDate, createDatetime, authorID) VALUES ('Das ist ein Postcontent am 2018-11-26 09:12:13!', '2018-11-26', '2018-11-26 09:32:13', 2);